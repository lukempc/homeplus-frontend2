import styled from 'styled-components';

export const DetailCard = styled.div`
  h2,
  span {
    font-family: 'Roboto';
    font-weight: 300;
  }
  max-width: 1000px;
  min-width: 300px;
  min-height: 800px;
  max-height: 800px;
  border-radius: 15px;
  position: relative;
  top: 2%;
  left: 50%;
  margin-bottom: 5%;
  transform: translateX(-50%);
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
  .longLine {
    width: 90%;
    border: 1px solid #f2f2f2;
    position: relative;
    left: 50%;
    transform: translateX(-50%);
  }
  > div {
    .cardTitle {
      display: flex;
      align-items: center;
      justify-content: space-around;
      width: 50%;
      margin-top: 3%;
    }
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 100%;
    margin-top: 2%;
  }
  .cardImportInfo {
    height: 75px;
    .shortLine {
      position: relative;
      top: 15%;
      tansform: translateY(-50%);
      height: 50px;
      border: 1px solid #f2f2f2;
    }
  }
  .cardOtherInfo {
    justify-content: space-between;
    align-items: start;
    > div {
      display: flex;
    }
    .taskInfo {
      display: inline;
      width: 60%;
      margin-left: 30px;
      .taskSubInfo {
        width: 100%;
        height: 90px;
        display: flex;
        justify-content: space-around;
      }
      .description {
        margin-top: 2%;
        padding: 2%;
      }
    }
    .commentArea {
      display: inline-grid;
      width: 35%;
      height: 550px;
      position: relative;
      justify-content: space-around;
      overflow: scroll;
      .commentBox {
        width: 100%;
        position: relative;
        justify-content: space-around;
      }
    }
  }
`;
