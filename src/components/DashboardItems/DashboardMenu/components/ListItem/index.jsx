import React from 'react';
import PropTypes from 'prop-types';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

const ListItem = ({ title, children, marginTop, selected, onClick }) => {
  return (
    <ListItemButton key={title} sx={{ marginTop: { marginTop } }} selected={selected === title} onClick={onClick}>
      <ListItemText primary={title} />
      {children}
    </ListItemButton>
  );
};

ListItem.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
  marginTop: PropTypes.string,
  selected: PropTypes.string,
  onClick: PropTypes.func,
};

export default ListItem;
