import * as React from 'react';
import Box from '@mui/material/Box';
import ListSubheader from '@mui/material/ListSubheader';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Avatar from '@mui/material/Avatar';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import FaceIcon from '@mui/icons-material/Face';
import PropTypes from 'prop-types';
import ListItem from './components/ListItem';

export default function SelectedListItem({ onMenuClick }) {
  const [selectedIndex, setSelectedIndex] = React.useState('Home');
  const handleListItemClick = (event, index) => {
    setSelectedIndex(index);
    if (index === 'Tasker Dashboard') toggleOpen();
    onMenuClick(index);
  };
  const toggleOpen = () => {
    setIsOpen(!isOpen);
  };
  const [isOpen, setIsOpen] = React.useState(false);

  return (
    <Box sx={{ display: 'flex', paddingTop: '15px' }}>
      <List
        sx={{
          width: '100%',
          maxWidth: 300,
          minWidth: 210,
          paddingLeft: 5,
          bgcolor: 'background.paper',
          marginTop: '30px',
        }}
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={<ListSubheader component="div" id="nested-list-subheader"></ListSubheader>}
      >
        <Box sx={{ display: 'flex', justifyContent: 'center' }}>
          <ListItemAvatar>
            <Avatar>
              <FaceIcon />
            </Avatar>
          </ListItemAvatar>
          <ListSubheader>Username</ListSubheader>
        </Box>

        {['Home', 'Posted Tasks'].map((title) => (
          <ListItem
            key={title}
            title={title}
            marginTop="20px"
            selected={selectedIndex}
            onClick={(event) => handleListItemClick(event, title)}
          />
        ))}
        <ListItem
          title="Tasker Dashboard"
          marginTop="20px"
          selected={selectedIndex}
          onClick={(event) => handleListItemClick(event, 'Tasker Dashboard')}
        >
          {isOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>

        <Collapse in={isOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {['Tasks', 'Profile', 'Payment'].map((title) => (
              <ListItemButton
                key={title}
                sx={{ pl: 4, marginTop: '8px' }}
                selected={selectedIndex === title}
                onClick={(event) => handleListItemClick(event, title)}
              >
                <ListItemText primary={title} />
              </ListItemButton>
            ))}
          </List>
        </Collapse>
        <ListItem
          title="User Profile"
          marginTop="20px"
          selected={selectedIndex}
          onClick={(event) => handleListItemClick(event, 'User Profile')}
        />
        <ListItem title="Log out" marginTop="40px" onClick={() => alert('logout')} />
      </List>
    </Box>
  );
}

SelectedListItem.propTypes = {
  onMenuClick: PropTypes.func.isRequired,
};
