import React from 'react';
import Box from '@mui/material/Box';
import SearchBar from '../../SearchBar';
import CustomTextButton from './CustomTextButton';
import TaskCard from '../../TaskCard';

const DashboardPostedTasks = () => {
  return (
    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '80%',
          alignItems: 'center',
          padding: '15px',
        }}
      >
        <SearchBar placeholder="Search your tasks" width="350px"></SearchBar>
        <CustomTextButton>All</CustomTextButton>
        <CustomTextButton>Open</CustomTextButton>
        <CustomTextButton>Assigned</CustomTextButton>
        <CustomTextButton>Completed</CustomTextButton>
      </Box>
      <Box sx={{ marginTop: '30px', paddingLeft: '15px' }}>
        <TaskCard></TaskCard>
      </Box>
    </Box>
  );
};

export default DashboardPostedTasks;
