import React from 'react';
import CustomButton from '../../../CustomButton';

const CustomTextButton = ({ children }) => (
  <CustomButton color="black" variant="text">
    {children}
  </CustomButton>
);

export default CustomTextButton;
