import * as React from 'react';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Typography from '@mui/material/Typography';

export default function ToggleButtons({ handleToggleButtons, defaultCategory }) {
  const [category, setCategory] = React.useState(defaultCategory);

  const handleToggle = (event, newCategory) => {
    if (newCategory !== null) {
      setCategory(newCategory);
      handleToggleButtons(newCategory);
    }
  };

  return (
    <ToggleButtonGroup value={category} exclusive onChange={handleToggle} aria-label="text category">
      <ToggleButton value="cleaning" aria-label="left aligned" style={{ width: '200px' }}>
        <Typography gutterBottom variant="span" component="div" align="center">
          Cleaning
        </Typography>
      </ToggleButton>
      <ToggleButton value="removal" aria-label="centered" style={{ width: '200px' }}>
        <Typography gutterBottom variant="span" component="div" align="center">
          Removal
        </Typography>
      </ToggleButton>
      <ToggleButton value="handy" aria-label="right aligned" style={{ width: '200px' }}>
        <Typography gutterBottom variant="span" component="div" align="center">
          Handyperson
        </Typography>
      </ToggleButton>
    </ToggleButtonGroup>
  );
}
