import { styled } from '@mui/system';

export const Card = styled('div')({
  width: 250,
  marginTop: 15,
  backgroundColor: '#C8D8E4',
  padding: 15,
  fontFamily: 'Roboto',
  borderRadius: 5,
  color: '#444',
});

export const Section = styled('div')({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
  '& >span': {
    fontSize: 12,
  },
});

export const User = styled('div')({
  display: 'flex',
  alignItems: 'center',
  maxHeight: 30,

  '& span': {
    padding: 10,
    fontWeight: 400,
  },
});

export const Message = styled('p')({
  wordWrap: 'break-word',
  marginTop: 15,
  marginLeft: 45,
  lineHeight: 1.4,
  fontWeight: 300,
});
