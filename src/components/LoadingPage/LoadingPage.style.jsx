import styled from 'styled-components';
import { loadingAnimation, loadingRotate } from '../../style';

export const LoaderContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 91vh;
  svg {
    position: relative;
    width: 150px;
    height: 150px;
    animation: ${loadingRotate} 2s linear infinite;

    circle {
      width: 100%;
      height: 100%;
      fill: none;
      stroke-width: 10;
      stroke: #52ab98;
      stroke-linecap: round;
      transform: translate(5px, 5px);
      stroke-dasharray: 440;
      stroke-dashoffset: 440;
      animation: ${loadingAnimation} 4s linear infinite;
    }
  }
`;
