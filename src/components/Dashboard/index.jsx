import React from 'react';
import Box from '@mui/material/Box';
import DashboardMenu from '../DashboardItems/DashboardMenu';
import DashboardTitleLine from '../DashboardItems/DashboardTitleLine';
import DashboardTaskerForm from '../DashboardItems/DashboardTaskerForm';
import DashboardPostedTasks from '../DashboardItems/DashboardPostedTasks';
import DashboardUserProfile from '../DashboardItems/DashboardUserProfile';
import DashboardPayment from '../DashboardItems/DashboardPayment';
import DashboardTaskerProfile from '../DashboardItems/DashboardTaskerProfile';
import DashboardHome from '../DashboardItems/DashboadHome';

const Dashboard = () => {
  const [title, setTitle] = React.useState('Home');
  const onMenuClick = (menuItem) => {
    setTitle(menuItem);
  };

  const editableItems = React.useMemo(() => ['User Profile', 'Payment', 'Profile'], []);
  const turnOnOffItem = React.useMemo(() => ['Tasker Dashboard'], []);

  const dashboardTitle = React.useMemo(() => {
    if (editableItems.includes(title)) {
      return <DashboardTitleLine title={title} edit />;
    } else if (turnOnOffItem.includes(title)) {
      return <DashboardTitleLine title={title} turn />;
    } else if (title) {
      return <DashboardTitleLine title={title} />;
    } else {
      return null;
    }
  }, [editableItems, title, turnOnOffItem]);

  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        width: '95%',
        justifyContent: 'center',
        color: '#444',
      }}
    >
      <DashboardMenu onMenuClick={onMenuClick} />
      <Box sx={{ width: '95%', marginLeft: '20px', paddingTop: '15px' }}>
        {dashboardTitle}

        {title === 'Home' && <DashboardHome />}
        {title === 'Posted Tasks' && <DashboardPostedTasks />}
        {title === 'Tasks' && <DashboardPostedTasks />}
        {title === 'Tasker Dashboard' && <DashboardTaskerForm />}
        {title === 'User Profile' && <DashboardUserProfile />}
        {title === 'Payment' && <DashboardPayment />}
        {title === 'Profile' && <DashboardTaskerProfile />}
      </Box>
    </Box>
  );
};

export default Dashboard;
