import React from 'react';
import HomePageInfo from './HomePageInfo';
import HomePageInstruction from './HomePageInstruction';
import Footer from '../../components/Footer';

const HomePage = () => (
  <>
    <HomePageInfo />
    <HomePageInstruction />
    <Footer />
  </>
);

export default HomePage;
